
# react-native-googlepay-button

## Getting started

`$ npm install react-native-googlepay-button --save`

### Mostly automatic installation

`$ react-native link react-native-googlepay-button`

### Manual installation


#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactnativegooglepaybutton.RNGooglePayButtonPackage;` to the imports at the top of the file
  - Add `new RNGooglePayButtonPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-googlepay-button'
  	project(':react-native-googlepay-button').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-googlepay-button/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-googlepay-button')
  	```


## Usage
```javascript
import RNGooglePayButton from 'react-native-googlepay-button';

...

render() {
  return <RNGooglePayButton />
}
```

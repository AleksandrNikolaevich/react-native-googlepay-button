import * as React from "react";
import { requireNativeComponent, TouchableOpacity, TouchableOpacityProperties, Platform, View } from 'react-native';

const RNGPayButton: any = Platform.select({android: requireNativeComponent('GooglePayButtonImageView'), default: View});

export enum GooglePayButtonType {
    WHITE_GOOGLE_BUTTON = "WHITE_GOOGLE_BUTTON",
    WHITE_GOOGLE_BUTTON_SHADOW = "WHITE_GOOGLE_BUTTON_SHADOW",
    WHITE_BUY_WITH_GOOGLE_BUTTON = "WHITE_BUY_WITH_GOOGLE_BUTTON",
    WHITE_BUY_WITH_GOOGLE_BUTTON_SHADOW = "WHITE_BUY_WITH_GOOGLE_BUTTON_SHADOW",
    BLACK_GOOGLE_BUTTON = "BLACK_GOOGLE_BUTTON",
    BLACK_BUY_WITH_GOOGLE_BUTTON = "BLACK_BUY_WITH_GOOGLE_BUTTON"
}

type ImageProps = { 
    type: GooglePayButtonType,
    width?: number,
    height?: number
}
export type GooglePayButtonProps = TouchableOpacityProperties & ImageProps;
const GPayButton: React.FC<GooglePayButtonProps> = ({type, width, height, ...props}) => {
    if(Platform.OS === "ios") return null;
    return (
        <TouchableOpacity {...props}>
            <RNGPayButton type={type} style={{ width, height }}/>
        </TouchableOpacity>
    )
}

export default GPayButton;

import * as React from "react";
import { TouchableOpacityProperties } from 'react-native';
export declare enum GooglePayButtonType {
    WHITE_GOOGLE_BUTTON = "WHITE_GOOGLE_BUTTON",
    WHITE_GOOGLE_BUTTON_SHADOW = "WHITE_GOOGLE_BUTTON_SHADOW",
    WHITE_BUY_WITH_GOOGLE_BUTTON = "WHITE_BUY_WITH_GOOGLE_BUTTON",
    WHITE_BUY_WITH_GOOGLE_BUTTON_SHADOW = "WHITE_BUY_WITH_GOOGLE_BUTTON_SHADOW",
    BLACK_GOOGLE_BUTTON = "BLACK_GOOGLE_BUTTON",
    BLACK_BUY_WITH_GOOGLE_BUTTON = "BLACK_BUY_WITH_GOOGLE_BUTTON"
}
declare type ImageProps = {
    type: GooglePayButtonType;
    width?: number;
    height?: number;
};
export declare type GooglePayButtonProps = TouchableOpacityProperties & ImageProps;
declare const GPayButton: React.FC<GooglePayButtonProps>;
export default GPayButton;

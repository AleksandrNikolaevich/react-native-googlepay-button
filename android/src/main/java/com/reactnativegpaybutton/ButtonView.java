package com.reactnativegpaybutton;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.reactnativegooglepaybutton.R;

import javax.annotation.Nullable;

public class ButtonView  extends FrameLayout {
    LayoutInflater inflater;

    public ButtonView(Context context) {
       super(context);
        inflater = LayoutInflater.from(context);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

    public void setType(@Nullable String type){
        View view;
        removeAllViews();
        Log.i("Test", "Set type button:" + type);
        switch (type){
            case "WHITE_GOOGLE_BUTTON":
                view = inflater.inflate(
                        R.layout.white_googlepay_button_no_shadow, null);
                break;
            case "WHITE_GOOGLE_BUTTON_SHADOW":
                view = inflater.inflate(
                        R.layout.white_googlepay_button, null);
                break;
            case "WHITE_BUY_WITH_GOOGLE_BUTTON":
                view = inflater.inflate(
                        R.layout.white_buy_with_googlepay_button_no_shadow, null);
                break;
            case "WHITE_BUY_WITH_GOOGLE_BUTTON_SHADOW":
                view = inflater.inflate(
                        R.layout.white_buy_with_googlepay_button, null);
                break;
            case "BLACK_GOOGLE_BUTTON":
                view = inflater.inflate(
                        R.layout.googlepay_button, null);
                break;
            case "BLACK_BUY_WITH_GOOGLE_BUTTON":
                view = inflater.inflate(
                        R.layout.buy_with_googlepay_button, null);
                break;
            default:
                view = inflater.inflate(
                        R.layout.googlepay_button, null);
        }
        addView(view);
    }
}

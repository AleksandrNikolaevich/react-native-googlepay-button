package com.reactnativegooglepaybutton;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.reactnativegpaybutton.ButtonView;

import java.util.List;

import javax.annotation.Nullable;

public class RNGooglePayButtonImageManager extends SimpleViewManager<ButtonView> {


    public static final String REACT_CLASS = "GooglePayButtonImageView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected ButtonView createViewInstance(ThemedReactContext reactContext) {

        return new ButtonView(reactContext);
    }

    @ReactProp(name = "type")
    public void setType(ButtonView view, @Nullable String type) {
        view.setType(type);

    }
}

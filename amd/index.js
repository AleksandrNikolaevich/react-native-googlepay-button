var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
define(["require", "exports", "react", "react-native"], function (require, exports, React, react_native_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.GooglePayButtonType = void 0;
    React = __importStar(React);
    var RNGPayButton = react_native_1.Platform.select({ android: react_native_1.requireNativeComponent('GooglePayButtonImageView'), default: react_native_1.View });
    var GooglePayButtonType;
    (function (GooglePayButtonType) {
        GooglePayButtonType["WHITE_GOOGLE_BUTTON"] = "WHITE_GOOGLE_BUTTON";
        GooglePayButtonType["WHITE_GOOGLE_BUTTON_SHADOW"] = "WHITE_GOOGLE_BUTTON_SHADOW";
        GooglePayButtonType["WHITE_BUY_WITH_GOOGLE_BUTTON"] = "WHITE_BUY_WITH_GOOGLE_BUTTON";
        GooglePayButtonType["WHITE_BUY_WITH_GOOGLE_BUTTON_SHADOW"] = "WHITE_BUY_WITH_GOOGLE_BUTTON_SHADOW";
        GooglePayButtonType["BLACK_GOOGLE_BUTTON"] = "BLACK_GOOGLE_BUTTON";
        GooglePayButtonType["BLACK_BUY_WITH_GOOGLE_BUTTON"] = "BLACK_BUY_WITH_GOOGLE_BUTTON";
    })(GooglePayButtonType = exports.GooglePayButtonType || (exports.GooglePayButtonType = {}));
    var GPayButton = function (_a) {
        var type = _a.type, width = _a.width, height = _a.height, props = __rest(_a, ["type", "width", "height"]);
        if (react_native_1.Platform.OS === "ios")
            return null;
        return (React.createElement(react_native_1.TouchableOpacity, __assign({}, props),
            React.createElement(RNGPayButton, { type: type, style: { width: width, height: height } })));
    };
    exports.default = GPayButton;
});
